// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "Tradewise",
    products: [
        .library(name: "Tradewise", targets: ["Tradewise"])
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", .exact("4.9.0")),
    ],
    targets: [
        .target(name: "Tradewise", dependencies: [
          "Alamofire",
        ], path: "Sources")
    ]
)
