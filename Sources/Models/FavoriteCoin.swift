//
// Generated by SwagGen
// https://github.com/yonaskolb/SwagGen
//

import Foundation

public class FavoriteCoin: APIModel {

    public var coinId: ID?

    public init(coinId: ID? = nil) {
        self.coinId = coinId
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        coinId = try container.decodeIfPresent("coinId")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(coinId, forKey: "coinId")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? FavoriteCoin else { return false }
      guard self.coinId == object.coinId else { return false }
      return true
    }

    public static func == (lhs: FavoriteCoin, rhs: FavoriteCoin) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
