//
// Generated by SwagGen
// https://github.com/yonaskolb/SwagGen
//

import Foundation

public class CreditInfo: APIModel {

    public var creditInfoId: ID?

    public var free: Int?

    public var locked: Int?

    public var updatedAt: DateTime?

    public var used: Int?

    public init(creditInfoId: ID? = nil, free: Int? = nil, locked: Int? = nil, updatedAt: DateTime? = nil, used: Int? = nil) {
        self.creditInfoId = creditInfoId
        self.free = free
        self.locked = locked
        self.updatedAt = updatedAt
        self.used = used
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        creditInfoId = try container.decodeIfPresent("creditInfoId")
        free = try container.decodeIfPresent("free")
        locked = try container.decodeIfPresent("locked")
        updatedAt = try container.decodeIfPresent("updatedAt")
        used = try container.decodeIfPresent("used")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(creditInfoId, forKey: "creditInfoId")
        try container.encodeIfPresent(free, forKey: "free")
        try container.encodeIfPresent(locked, forKey: "locked")
        try container.encodeIfPresent(updatedAt, forKey: "updatedAt")
        try container.encodeIfPresent(used, forKey: "used")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? CreditInfo else { return false }
      guard self.creditInfoId == object.creditInfoId else { return false }
      guard self.free == object.free else { return false }
      guard self.locked == object.locked else { return false }
      guard self.updatedAt == object.updatedAt else { return false }
      guard self.used == object.used else { return false }
      return true
    }

    public static func == (lhs: CreditInfo, rhs: CreditInfo) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
